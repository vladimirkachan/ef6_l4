﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWModel
{
    class Program
    {
        static void Main(string[] args)
        {
            using AdventureWorks2019Entities db = new();
            //LazyLoad(db);
            //StrictLoad(db);
            EagerLoad(db);
            Console.ReadKey();

        }
        static void EagerLoad(AdventureWorks2019Entities db)
        {
            db.Configuration.LazyLoadingEnabled = false;
            var query = from order in db.SalesOrderHeaders.Include("Customer")
                        where order.SalesOrderID < 45000
                        select order;
            Console.WriteLine(query);
            var ordersList = query.ToList();
             foreach (var order in ordersList)
            //Нет запросов к БД!!!
                Console.WriteLine(order.Customer.AccountNumber);

        }
        static void StrictLoad(AdventureWorks2019Entities db)
        {
            db.Configuration.LazyLoadingEnabled = false;
            db.Customers.Load();
            var query = from order in db.SalesOrderHeaders
                        where order.SalesOrderID < 45000
                        select order;
            Console.WriteLine(query);
            var ordersList = query.ToList();
            foreach (var order in ordersList.Where(order => order.Customer != null)) 
                Console.WriteLine(order.Customer.AccountNumber);

        }
        static void LazyLoad(AdventureWorks2019Entities db)
        { //Code-First defaults: true, DB/ModelFirst - See EDMX Props
            db.Configuration.LazyLoadingEnabled = true;
            var query = from order in db.SalesOrderHeaders 
                        where order.SalesOrderID < 45000 
                        select order;
            Console.WriteLine(query);
            var ordersList = query.ToList();
            foreach (var order in ordersList.Where(order => order.Customer != null))
                Console.WriteLine("{0}.{1}", order.Customer.CustomerID, order.Customer.AccountNumber);
        }
    }
}
